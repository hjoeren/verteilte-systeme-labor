package de.hska.iwi.vslab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.hska.iwi.vslab.models.Category;
import de.hska.iwi.vslab.models.CategoryDao;

@RestController
public class CategoryController {

	@Autowired
	private CategoryDao categoryDao;

	@RequestMapping(value = "/categories", method = RequestMethod.GET)
	public ResponseEntity<?> getCategories() {
		return new ResponseEntity<>(categoryDao.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/categories/{categoryId}", method = RequestMethod.GET)
	public ResponseEntity<?> getCategory(@PathVariable long categoryId) {
		try {
			Category category = categoryDao.findById(categoryId);
			if (category != null) {
				return new ResponseEntity<>(category, HttpStatus.OK);
			}
		}
		catch (Exception e) {
			System.out.println("Error searching Category " + e);
		}

		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(value = "/categories", method = RequestMethod.PUT)
	public ResponseEntity<?> createCategory(@RequestParam(value = "name") String name) {
		try {
			Category category = categoryDao.findByName(name);
			if (category != null) {
				return new ResponseEntity<>(HttpStatus.CONFLICT);
			}
		}
		catch (Exception e) {
			System.out.println("Error searching Category " + e);
		}
		
		Category category = new Category(name);
		categoryDao.save(category);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	

	
	@RequestMapping(value = "/categories/{categoryId}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteCategory(@PathVariable long categoryId) {
		try {
			Category category = categoryDao.findById(categoryId);
			
			if (category != null) {
				categoryDao.delete(category);
				
				return new ResponseEntity<>(HttpStatus.OK);
			}
		}
		catch (Exception e) {
			System.out.println("Error deleting Category " + e);
		}

		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
}