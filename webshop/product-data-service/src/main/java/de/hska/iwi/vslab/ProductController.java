package de.hska.iwi.vslab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.hska.iwi.vslab.models.Category;
import de.hska.iwi.vslab.models.CategoryDao;
import de.hska.iwi.vslab.models.Product;
import de.hska.iwi.vslab.models.ProductDao;

@RestController
public class ProductController {
	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private CategoryDao categoryDao;

	
	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public ResponseEntity<?> getProducts() {
		return new ResponseEntity<>(productDao.findAll(), HttpStatus.OK);
	}

	@RequestMapping(value = "/products/category/{categoryId}", method = RequestMethod.GET)
	public ResponseEntity<?> getProductsByCategory(@PathVariable long categoryId) {
		return new ResponseEntity<>(productDao.findByCategoryId(categoryId), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/products/{productId}", method = RequestMethod.GET)
	public ResponseEntity<?> getProduct(@PathVariable long productId) {
		try {
			Product product = productDao.findById(productId);
			if (product != null) {
				return new ResponseEntity<>(product, HttpStatus.OK);
			}
		}
		catch (Exception e) {
			System.out.println("Error searching Product " + e);
		}

		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}
	
	
	@RequestMapping(value = "/products", method = RequestMethod.PUT)
	public ResponseEntity<?> createProduct(@RequestParam(value = "name") String name,
			@RequestParam(value = "price") double price,
			@RequestParam(value = "categoryId") long categoryId,
			@RequestParam(value = "details") String details) {
		try {
			Product product = productDao.findByName(name);
			if (product != null) {
				return new ResponseEntity<>(HttpStatus.CONFLICT);
			}
		}
		catch (Exception e) {
			System.out.println("Error searching Product " + e);
		}
		try {
			Category category = categoryDao.findById(categoryId);
			if (category == null) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		}
		catch (Exception e) {
			System.out.println("Error with Category ID " + e);
		}
		
		Product product = new Product(name, price, categoryId, details);
		productDao.save(product);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	

	
	@RequestMapping(value = "/products/{productId}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteProduct(@PathVariable long productId) {
		try {
			Product product = productDao.findById(productId);
			
			if (product != null) {
				productDao.delete(product);
				
				return new ResponseEntity<>(HttpStatus.OK);
			}
		}
		catch (Exception e) {
			System.out.println("Error deleting Product " + e);
		}

		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}