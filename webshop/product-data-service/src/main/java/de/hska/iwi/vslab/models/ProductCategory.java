package de.hska.iwi.vslab.models;

public class ProductCategory {
	Product product;
	Category category;
	
	public ProductCategory(Product product, Category category) {
		this.product = product;
		this.category = category;
	}

	public Product getProduct() {
		return product;
	}

	public Category getCategory() {
		return category;
	}
	
	
}