package de.hska.iwi.vslab.models;

public class ProductsCategories {
	Iterable<Product> products;
	Iterable<Category> categories;
	
	public ProductsCategories(Iterable<Product> product, Iterable<Category> category) {
		this.products = product;
		this.categories = category;
	}

	public Iterable<Product> getProduct() {
		return products;
	}

	public Iterable<Category> getCategory() {
		return categories;
	}
	
	
}