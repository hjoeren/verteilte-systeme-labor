package de.hska.iwi.vslab.models;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

@Transactional
public interface CategoryDao extends CrudRepository<Category, Long> {
	public Iterable<Category> findAll();
	
	public Category findByName(String name);

	public Category findById(long id);
}
