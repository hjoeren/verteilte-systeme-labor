package de.hska.iwi.vslab.models;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

@Transactional
public interface ProductDao extends CrudRepository<Product, Long> {
	public Product findByName(String name);
	public Product findById(long userId);
	
	public Iterable<Product> findByCategoryId(long id);
}
