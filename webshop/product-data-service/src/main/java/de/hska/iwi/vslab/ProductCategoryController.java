package de.hska.iwi.vslab;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.hska.iwi.vslab.models.Category;
import de.hska.iwi.vslab.models.CategoryDao;
import de.hska.iwi.vslab.models.Product;
import de.hska.iwi.vslab.models.ProductCategory;
import de.hska.iwi.vslab.models.ProductsCategories;
import de.hska.iwi.vslab.models.ProductDao;

@RestController
public class ProductCategoryController {

	@Autowired
	private ProductDao productDao;
	
	@Autowired
	private CategoryDao categoryDao;

	@RequestMapping(value = "/products-categories", method = RequestMethod.GET)
	public ResponseEntity<?> getCategories() {
		
		ProductsCategories productCategory = new ProductsCategories(productDao.findAll(), categoryDao.findAll());
		return new ResponseEntity<>(productCategory, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/products-categories/{productId}", method = RequestMethod.GET)
	public ResponseEntity<?> getCategory(@PathVariable long productId) {
		try {
			
			Product product = productDao.findById(productId);
			
			Category category = categoryDao.findById(product.getCategoryId());
			
			if (product != null && category != null) {
				return new ResponseEntity<>(new ProductCategory(product, category), HttpStatus.OK);
			}
		}
		catch (Exception e) {
			System.out.println("Error searching Category " + e);
		}

		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}	
	

	@RequestMapping(value = "/products-categories/search", method = RequestMethod.GET)
	public ResponseEntity<?> getProductsFiltered(
			@RequestParam(value = "searchdescription") String searchDescription,
			@RequestParam(value = "searchminprice") double searchMinPrice,
			@RequestParam(value = "searchmaxprice") double searchMaxPrice) {
		
		Iterable<Product> products = productDao.findAll();
		
		ArrayList<Product> newProducts = new ArrayList<Product>();
		for (Product product: products) {
			boolean containsString = true;
			boolean priceHigher = true;
			boolean priceLower = true;
			
			if (searchDescription != null && !searchDescription.isEmpty() && !"*".equals(searchDescription)) {
				if (!product.getName().contains(searchDescription) && !product.getDetails().contains(searchDescription)) {
					containsString = false;
				}
			}
			
			if (searchMinPrice != -1 && product.getPrice() < searchMinPrice) {
				priceHigher = false;
			}
			
			if (searchMaxPrice != -1 && product.getPrice() > searchMaxPrice) {
				priceLower = false;
			}
			
			if (containsString && priceHigher && priceLower) {
				newProducts.add(product);
			}
		}
		ProductsCategories productCategory = new ProductsCategories(newProducts, categoryDao.findAll());
		
		return new ResponseEntity<>(productCategory, HttpStatus.OK);
	}
	
}