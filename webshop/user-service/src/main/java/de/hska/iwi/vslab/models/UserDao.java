package de.hska.iwi.vslab.models;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

@Transactional
public interface UserDao extends CrudRepository<User, Long> {
	public User findByUsername(String username);
	public User findById(long userId);
}
