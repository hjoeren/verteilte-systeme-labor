package de.hska.iwi.vslab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.hska.iwi.vslab.models.User;
import de.hska.iwi.vslab.models.UserDao;

@RestController
public class UserController {

	@Autowired
	private UserDao userDao;
	
	@RequestMapping(value = "/user/{username}", method = RequestMethod.GET)
	public ResponseEntity<?> getUser(@PathVariable String username) {
		try {
			User user = userDao.findByUsername(username);
			if (user != null) {
				System.out.println("Exists");
				return new ResponseEntity<>(user, HttpStatus.OK);
			}
		}
		catch (Exception e) {
			System.out.println("Error searching User " + e);
		}
		System.out.println("Does not exists");
		return new ResponseEntity<>(new User(), HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(value = "/user/register", method = RequestMethod.PUT)
	public ResponseEntity<?> createUser(
			@RequestParam(value = "username") String username,
			@RequestParam(value = "password") String password, 
			@RequestParam(value = "firstname") String firstname,
			@RequestParam(value = "lastname") String lastname,
			@RequestParam(value = "role") int role
	) {
		try {
			User user = userDao.findByUsername(username);
			
			if (user != null) {
				return new ResponseEntity<>(HttpStatus.CONFLICT);
			}
		}
		catch (Exception e) {
			System.out.println("Error searching User " + e);
		}
		
		User user = new User(username, password, firstname, lastname, role);
		userDao.save(user);
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
}