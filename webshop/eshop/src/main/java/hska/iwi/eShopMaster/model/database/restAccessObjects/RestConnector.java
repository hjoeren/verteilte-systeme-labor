package hska.iwi.eShopMaster.model.database.restAccessObjects;

import org.springframework.web.client.RestTemplate;

public class RestConnector {

    private RestTemplate restTemplate;
    public static String URL = "http://localhost:8078/";
    public RestConnector(){
        restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new MyResponseErrorHandler());
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

}
