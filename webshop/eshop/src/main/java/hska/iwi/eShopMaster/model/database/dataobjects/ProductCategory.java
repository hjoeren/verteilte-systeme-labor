package hska.iwi.eShopMaster.model.database.dataobjects;

public class ProductCategory {
	Product product;
	Category category;
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
}