package hska.iwi.eShopMaster.model.businessLogic.manager.impl;

import hska.iwi.eShopMaster.model.businessLogic.manager.CategoryManager;
import hska.iwi.eShopMaster.model.database.dataobjects.Category;
import hska.iwi.eShopMaster.model.database.restAccessObjects.RestCategory;

import java.util.List;

public class CategoryManagerImpl implements CategoryManager {
	private RestCategory helper;

	public CategoryManagerImpl() {
		helper = new RestCategory();
	}

	@Override
	public List<Category> getCategories() {
		return helper.getObjectList();
	}

	@Override
	public Category getCategory(int id) {
		return helper.getObjectById(id);
	}

	@Override
	public void addCategory(String name) {
		Category cat = new Category(name);
		helper.saveObject(cat);
	}

	@Override
	public void delCategory(Category cat) {
		helper.deleteById(cat.getId());
	}
	
	public Category getCategoryByIdFromCategoryList(List<Category> categories, int categoryId) {
		for (Category category: categories) {
			if (category.getId() == categoryId) {
				return category;
			}
		}
		return null;
	}

	@Override
	public void delCategoryById(int id) {
		helper.deleteById(id);
	}
}
