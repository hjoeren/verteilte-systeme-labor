package hska.iwi.eShopMaster.model.businessLogic.manager;

import hska.iwi.eShopMaster.model.database.dataobjects.User;

//@RequestMapping(value = "/user/register", method = RequestMethod.PUT)
//@RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)

public interface UserManager {

    public static int ROLE_USER = 1;
    public static int ROLE_ADMIN = 2;

    public void registerUser(String username, String name, String lastname, String password, int role);
    
    public User getUserByUsername(String username);
    
    public boolean deleteUserById(int id);
    
    public int getRoleByLevel(int level);
    
    public boolean doesUserAlreadyExist(String username);
}
