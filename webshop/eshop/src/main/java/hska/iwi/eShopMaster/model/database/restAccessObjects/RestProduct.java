package hska.iwi.eShopMaster.model.database.restAccessObjects;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import hska.iwi.eShopMaster.model.database.dataobjects.Product;
import hska.iwi.eShopMaster.model.database.dataobjects.ProductCategory;
import hska.iwi.eShopMaster.model.database.dataobjects.ProductsCategories;

import java.io.IOException;
import java.util.List;

public class RestProduct {
    private static final String GET_OR_DELETE = RestConnector.URL + "product-data-service/products/";
    private static final String GET_OR_PUT = RestConnector.URL + "product-data-service/products";
    private static final String GET_PRODUCTS_AND_CATEGORIES_URL = RestConnector.URL + "product-data-service/products-categories";
    

    private RestConnector rest = new RestConnector();
    private ObjectMapper mapper = new ObjectMapper();

    public List<Product> getObjectList() {
        List<Product> products = null;
        try {
            String response = rest.getRestTemplate().getForObject(GET_OR_PUT, String.class);
            products = mapper.readValue(response, new TypeReference<List<Product>>(){});
        } catch (IOException e) {
        	System.out.println(e);
        }
        return products;
    }

    public Product getProductAndCategoryById(int id) {
        Product product = null;
        try {
            String response = rest.getRestTemplate().getForObject(GET_PRODUCTS_AND_CATEGORIES_URL + "/" + Integer.toString(id), String.class);
            
            ProductCategory productsCategories = mapper.readValue(response, new TypeReference<ProductCategory>(){}); 
            product = productsCategories.getProduct();
            product.updateCategory(productsCategories.getCategory());
        } catch (IOException e) {
        	System.out.println(e);
        }
        return product;
    }
    public Product getObjectById(int id) {
        Product product = null;
        try {
            String response = rest.getRestTemplate().getForObject(GET_OR_DELETE + Integer.toString(id), String.class);
            product = mapper.readValue(response, Product.class);
        } catch (IOException e) {
        	System.out.println(e);
        }
        return product;
    }
    public Product getObjectByName(String name) {
        for (Product product : getObjectList()) {
            if(product.getName().equals(name)){
                return product;
            }
        }
        return null;
    }
    
    public void saveObject(Product product) {
    	System.out.println("SaveObject in RestProduct");
        try {
            String json = mapper.writeValueAsString(product);
            String path = "?name=" + product.getName() + "&price=" + product.getPrice() + "&categoryId=" + product.getCategoryId() + "&details=" + product.getDetails();
            rest.getRestTemplate().put(GET_OR_PUT + path, json);
        } catch (JsonProcessingException e) {
        	System.out.println(e);
        }
    }

    public void deleteById(int id) {
    	System.out.println(GET_OR_DELETE + Integer.toString(id));
        rest.getRestTemplate().delete(GET_OR_DELETE + Integer.toString(id));
    }

	public ProductsCategories getProductsAndCategories() {
		ProductsCategories productsCategories = null;
        try {
            String response = rest.getRestTemplate().getForObject(GET_PRODUCTS_AND_CATEGORIES_URL, String.class);
            productsCategories = mapper.readValue(response, new TypeReference<ProductsCategories>(){});
        } catch (IOException e) {
        	System.out.println(e);
        }
        return productsCategories;
	}

	public ProductsCategories getAllProductsAndCategoriesFiltered(String searchDescription, Double searchMinPrice,
			Double searchMaxPrice) {
		ProductsCategories productsCategories = null;
        try {
        	String path = "/search?searchdescription=" + ((searchDescription == null || searchDescription.isEmpty()) ? "*" : searchDescription);
    		path += "&searchminprice=" + (searchMinPrice == null ? -1 : searchMinPrice);
    		path += "&searchmaxprice=" + (searchMaxPrice == null ? -1 : searchMaxPrice);
    		    System.out.println("DEBUG: " + path);
            String response = rest.getRestTemplate().getForObject(GET_PRODUCTS_AND_CATEGORIES_URL + path, String.class);
            productsCategories = mapper.readValue(response, new TypeReference<ProductsCategories>(){});
        } catch (IOException e) {
        	System.out.println(e);
        }
        return productsCategories;
	}
	
	public List<Product> getProductsByCategoryId(int categoryId) {
		List<Product> products = null;
        
        try {
        	String response = rest.getRestTemplate().getForObject(GET_OR_DELETE + "category/" + Integer.toString(categoryId), String.class);
			products = mapper.readValue(response, new TypeReference<List<Product>>(){});
		} catch (IOException e) {
			System.out.println(e);
		}
		return products;
	}
}
