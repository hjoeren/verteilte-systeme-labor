package hska.iwi.eShopMaster.model.database.restAccessObjects;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import hska.iwi.eShopMaster.model.database.dataobjects.Category;

import java.io.IOException;
import java.util.List;

public class RestCategory {

    private static final String FOR_ID = RestConnector.URL + "product-data-service/categories/";
    private static final String PUT = RestConnector.URL + "product-data-service/categories";
    private RestConnector rest = new RestConnector();
    private ObjectMapper mapper = new ObjectMapper();

    public List<Category> getObjectList() {
        List<Category> categories = null;
        try {
            String response = rest.getRestTemplate().getForObject(PUT, String.class);
            categories = mapper.readValue(response, new TypeReference<List<Category>>(){});
        } catch (IOException e) {
        }
        return categories;
    }

    public Category getObjectById(int id) {
        Category category = null;
        try {
            String response = rest.getRestTemplate().getForObject(FOR_ID + Integer.toString(id), String.class);
            category = mapper.readValue(response, Category.class);
        } catch (IOException e) {
        }
        return category;
    }

    public Category getObjectByName(String name) {
        for (Category category : getObjectList()) {
            if(category.getName().equals(name)){
                return category;
            }
        }
        return null;
    }

    public void saveObject(Category cat) {
        String json = null;
        try {
            json = mapper.writeValueAsString(cat);
        } catch (JsonProcessingException e) {
        	System.out.println(e);
        }
        System.out.println(json);
        rest.getRestTemplate().put(PUT + "?id=" + 0 + "&name=" + cat.getName(), "");
    }

    public void deleteById(int id) {
        rest.getRestTemplate().delete(FOR_ID + Integer.toString(id));
    }
}
