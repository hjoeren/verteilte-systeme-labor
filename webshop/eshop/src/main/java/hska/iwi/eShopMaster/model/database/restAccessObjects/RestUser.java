package hska.iwi.eShopMaster.model.database.restAccessObjects;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import hska.iwi.eShopMaster.model.database.dataobjects.User;

import java.io.IOException;

public class RestUser {

	private static final String PUT = RestConnector.URL + "user-service/user/register";
	private static final String GET = RestConnector.URL + "user-service/user/";
	private RestConnector rest = new RestConnector();
	private ObjectMapper mapper = new ObjectMapper();

	public void saveObject(User user) {
		try {
			String json = mapper.writeValueAsString(user);
			System.out.println("JSON" + json);
			String path = "?id=" + 0 + "&username=" + user.getUsername() + "&password=" + user.getPassword() + "&firstname=" + user.getFirstname() + "&lastname=" + user.getLastname() + "&role=" + user.getRole();
			rest.getRestTemplate().put(PUT + path, "");
			
			
		/*	
			
		    HttpHeaders headers = new HttpHeaders(); 
		    HttpEntity<?> entity = new HttpEntity<>(json, headers); 
		    ResponseEntity<Object> response = rest.getRestTemplate().exchange(PUT + path, HttpMethod.PUT, entity, Object.class);
		    // check the response, e.g. Location header,  Status, and body
		    System.out.println("location: " + response.getHeaders().getLocation());
		    System.out.println("statusCode: " + response.getStatusCode());
//		    System.out.println("responseBody: " + response.getBody());
			*/
			
			
			
			
			
			
			System.out.println("Json erfolgreich");
		} catch (JsonProcessingException e) {
			System.out.println(e);
		}
	}

	public User getUserByUsername(String username) {
		User user = null;
		try {
			
			String response = rest.getRestTemplate().getForObject(GET + username, String.class);

			if (response == null) {
				return null;
			}
			user = mapper.readValue(response, User.class);
		} catch (IOException e) {
			System.out.println(e);
		}
		
		if (user == null || user.getUsername() == null) {
			return null;
		}
		
		return user;
	}

	public void deleteObject(User user) {
		rest.getRestTemplate().delete(GET + user.getUsername());
	}
}
