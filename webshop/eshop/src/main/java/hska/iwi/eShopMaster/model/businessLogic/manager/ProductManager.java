package hska.iwi.eShopMaster.model.businessLogic.manager;

import hska.iwi.eShopMaster.model.database.dataobjects.Product;
import hska.iwi.eShopMaster.model.database.dataobjects.ProductsCategories;

import java.util.List;

public interface ProductManager {

	public List<Product> getProducts();

	public Product getProductById(int id);

	public int addProduct(String name, double price, int categoryId, String details);

	public ProductsCategories getProductsForSearchValues(String searchValue, Double searchMinPrice, Double searchMaxPrice);
	
    public void deleteProductById(int id);
    
    public ProductsCategories getProductsAndCategories();
    
    public Product getProductAndCategory(int id);
    
    public List<Product> getProductsByCategory(int id);
	
}
