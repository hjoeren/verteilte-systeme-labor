package hska.iwi.eShopMaster.model.businessLogic.manager.impl;

import hska.iwi.eShopMaster.model.businessLogic.manager.ProductManager;
import hska.iwi.eShopMaster.model.database.dataobjects.Product;
import hska.iwi.eShopMaster.model.database.dataobjects.ProductsCategories;
import hska.iwi.eShopMaster.model.database.restAccessObjects.RestProduct;

import java.util.List;

public class ProductManagerImpl implements ProductManager {
	private RestProduct helper;

	public ProductManagerImpl() {
		helper = new RestProduct();
	}

	@Override
	public List<Product> getProducts() {
		return helper.getObjectList();
	}
	@Override
	public ProductsCategories getProductsForSearchValues(String searchDescription, Double searchMinPrice, Double searchMaxPrice) {
		return helper.getAllProductsAndCategoriesFiltered(searchDescription, searchMinPrice, searchMaxPrice); 
	}
	@Override
	public Product getProductById(int id) {
		return helper.getObjectById(id);
	}
	@Override
	public int addProduct(String name, double price, int categoryId, String details) {
		int productId = -1;

		Product product;
		if (details == null || "".equals(details)) {
			product = new Product(name, price, categoryId);
		} else {
			product = new Product(name, price, categoryId, details);
		}
		helper.saveObject(product);
		productId = helper.getObjectByName(product.getName()).getId();

		return productId;
	}
	
	@Override
	public void deleteProductById(int id) {
		helper.deleteById(id);
	}

	@Override
	public ProductsCategories getProductsAndCategories() {
		return helper.getProductsAndCategories();
	}

	@Override
	public Product getProductAndCategory(int id) {
		return helper.getProductAndCategoryById(id);
	}

	@Override
	public List<Product> getProductsByCategory(int id) {
		return helper.getProductsByCategoryId(id);
	}

}
