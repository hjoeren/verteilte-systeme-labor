package hska.iwi.eShopMaster.model.businessLogic.manager.impl;

import hska.iwi.eShopMaster.model.businessLogic.manager.UserManager;
import hska.iwi.eShopMaster.model.database.dataobjects.User;
import hska.iwi.eShopMaster.model.database.restAccessObjects.RestUser;

/**
 * 
 * @author knad0001
 */

//@RequestMapping(value = "/user/register", method = RequestMethod.PUT)
//@RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)

public class UserManagerImpl implements UserManager {
	RestUser helper;
	
	public UserManagerImpl() {
		helper = new RestUser();
	}

	
	@Override
	public void registerUser(String username, String name, String lastname, String password, int role) {

		User user = new User(username, name, lastname, password, role);

		helper.saveObject(user);
	}

	
	@Override
	public User getUserByUsername(String username) {
		if (username == null || username.equals("")) {
			return null;
		}
		return helper.getUserByUsername(username);
	}

	@Override
	public boolean deleteUserById(int id) {
		User user = new User();
		user.setId(id);
		helper.deleteObject(user);
		return true;
	}

	@Override
	public boolean doesUserAlreadyExist(String username) {
		
    	User dbUser = this.getUserByUsername(username);
    	return dbUser != null;
	}
	

	public boolean validate(User user) {
		return !(user.getFirstname().isEmpty() || user.getPassword().isEmpty() || user.getRole() == 0 || user.getLastname() == null || user.getUsername() == null);
	}


	@Override
	public int getRoleByLevel(int level) {
		// TODO Auto-generated method stub
		return level;
	}

}
