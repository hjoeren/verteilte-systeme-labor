package hska.iwi.eShopMaster.model.database.dataobjects;

import java.util.List;

public class ProductsCategories {
	List<Product> product;
	List<Category> category;
	
	/*
	public ProductsCategories(List<Product> product, List<Category> category) {
		this.product = product;
		this.category = category;
	}
	*/

	public List<Product> getProduct() {
		return product;
	}

	public void setProduct(List<Product> product) {
		this.product = product;
	}

	public List<Category> getCategory() {
		return category;
	}

	public void setCategory(List<Category> category) {
		this.category = category;
	}

}