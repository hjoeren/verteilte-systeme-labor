package hska.iwi.eShopMaster.model.database.dataobjects;

import java.util.Set;

/**
 * This class contains details about categories.
 */
public class Category implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	
	private boolean deletable = true;
	

	public Category() {
	}

	public Category(String name) {
		this.name = name;
	}

	public Category(String name, Set<Product> products) {
		this.name = name;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDeletable() {
		return deletable;
	}

	public void setDeletable(boolean deletable) {
		this.deletable = deletable;
	}
}
