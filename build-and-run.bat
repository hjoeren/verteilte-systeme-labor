call mvn package -f webshop\pom.xml

start cmd /k call mvn spring-boot:run -f webshop\eureka-server\pom.xml
start cmd /k call mvn spring-boot:run -f webshop\user-service\pom.xml
start cmd /k call mvn spring-boot:run -f webshop\product-data-service\pom.xml
start cmd /k call mvn spring-boot:run -f webshop\gateway-service\pom.xml
start cmd /k call mvn spring-boot:run -f webshop\hystrix-dashboard\pom.xml
start cmd /k call mvn tomcat7:redeploy -f webshop\eshop\pom.xml


