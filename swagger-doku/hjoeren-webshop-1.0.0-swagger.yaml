---
swagger: "2.0"
info:
  version: "1.0.0"
  title: "webshop"
  description: "webshop"
schemes:
- "http"
- "https"
consumes:
- "application/json"
produces:
- "application/json"
paths:
  /user/{userId}:
    get:
      summary: "Returns the user data"
      parameters:
      - name: "userId"
        in: "path"
        description: "The id of the user"
        required: true
        type: "integer"
        format: "int32"
      responses:
        200:
          description: "OK"
          schema:
            $ref: "#/definitions/User"
        404:
          description: "The user was not found"
  /user/register:
    put:
      summary: "Registers a user"
      parameters:
      - name: "username"
        in: "query"
        description: "The username of the user"
        required: true
        type: "string"
      - name: "password"
        in: "query"
        description: "The password of the user"
        required: true
        type: "string"
      - name: "firstname"
        in: "query"
        description: "The first name of the user"
        required: true
        type: "string"
      - name: "lastname"
        in: "query"
        description: "The last name of the user"
        required: true
        type: "string"
      responses:
        200:
          description: "OK"
        409:
          description: "The username is already available"
  /products:
    get:
      summary: "Returns a list of products"
      parameters:
      - name: "name"
        in: "query"
        description: "The name of the product"
        required: false
        type: "string"
      - name: "category"
        in: "query"
        description: "The category of the product"
        required: false
        type: "string"
      responses:
        200:
          description: "OK"
          schema:
            $ref: "#/definitions/Products"
    put:
      summary: "Adds a product"
      parameters:
      - name: "name"
        in: "query"
        description: "The name of the product"
        required: true
        type: "string"
      - name: "description"
        in: "query"
        description: "The description of the product"
        required: false
        type: "string"
      - name: "categoryId"
        in: "query"
        description: "The id of the category"
        required: true
        type: "integer"
        format: "int32"
      - name: "price"
        in: "query"
        description: "The price of the product"
        required: true
        type: "number"
        format: "double"
      responses:
        200:
          description: "OK"
        409:
          description: "The Product already exists"
        400:
          description: "The Category ID is wrong"
  /products/{productId}:
    get:
      summary: "Returns a product"
      parameters:
      - name: "productId"
        in: "path"
        description: "The id of the product"
        required: true
        type: "integer"
        format: "int32"
      responses:
        200:
          description: "OK"
          schema:
            $ref: "#/definitions/Product"
        404:
          description: "The product was not found"
    delete:
      summary: "Deletes a product"
      parameters:
      - name: "productId"
        in: "path"
        description: "The id of the product"
        required: true
        type: "integer"
        format: "int32"
      responses:
        200:
          description: "OK"
        404:
          description: "The product was not found"
  /categories:
    get:
      summary: "Returns a list of catgeories"
      responses:
        200:
          description: "OK"
          schema:
            $ref: "#/definitions/Categories"
    put:
      summary: "Adds a category"
      parameters:
      - name: "name"
        in: "query"
        description: "The name of the category"
        required: true
        type: "string"
      responses:
        200:
          description: "OK"
        409:
          description: "The Product already exists"
  /categories/{categoryId}:
    get:
      summary: "Returns a category"
      parameters:
      - name: "categoryId"
        in: "path"
        description: "The id of the category"
        required: true
        type: "integer"
        format: "int32"
      responses:
        200:
          description: "OK"
          schema:
            $ref: "#/definitions/Category"
        404:
          description: "The category was not found"
    delete:
      summary: "Deletes a category"
      parameters:
      - name: "categoryId"
        in: "path"
        description: "The id of the category"
        required: true
        type: "integer"
        format: "int32"
      responses:
        200:
          description: "OK"
        404:
          description: "The category was not found"
definitions:
  User:
    required:
    - "id"
    - "username"
    - "firstname"
    - "lastname"
    properties:
      id:
        description: "The name of the user"
        type: "integer"
        format: "int32"
      username:
        description: "The username of the user"
        type: "string"
      firstname:
        description: "The firstname of the user"
        type: "string"
      lastname:
        description: "The lastname of the user"
        type: "string"
  Category:
    required:
    - "id"
    - "name"
    properties:
      id:
        description: "The id of the category"
        type: "integer"
        format: "int32"
      name:
        description: "The name of the category"
        type: "string"
      products:
        description: "The ids of the products"
        type: "array"
        items:
          type: "integer"
          format: "int32"
  Categories:
    type: "array"
    items:
      $ref: "#/definitions/Category"
  Product:
    required:
    - "id"
    - "name"
    - "price"
    - "categoryId"
    properties:
      id:
        description: "The id of the product"
        type: "integer"
        format: "int32"
      name:
        description: "The name of the product"
        type: "string"
      description:
        description: "The description of the product"
        type: "string"
      price:
        description: "The price of the product"
        type: "number"
        format: "double"
      categoryId:
        description: "The id of the category"
        type: "integer"
        format: "int32"
  Products:
    type: "array"
    items:
      $ref: "#/definitions/Product"
