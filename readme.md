TODO:
-- ggf. überflüssige Funktionen löschen
-- Diagramme überprüfen

Systemvoraussetzungen: Maven 3.3, (MySQL-) Datenbank, am besten einfach ein XAMPP installieren   


Um die Spring-Boot Anwendungen laufen zu lassen, unter Windows einfach die Batch-Datei im Repo-Root ausführen.   
Diese baut erst die einzelnen Anwendungen nacheinander und startet dann parallel die Anwendungen.   


Händisch geht das natürlich auch. Einfach für jede Anwendung eine Console starten und in den jeweiligen Modul-Ordner wechseln.   

Zum Bauen der Anwendung:    
"mvn package"   

Zum Starten der Anwendung:   
"mvn spring-boot:run"   

Die einzelnen Anwendungen laufen auf unterschiedlichen Ports. Zu finden sind sie unter:   
User-Service: http://localhost:8070   
Gateway-Service: http://localhost:8080   
Product-Service: http://localhost:8090   

getestet werden kann die Funktionalität unter:   
http://localhost:8070/hello   
http://localhost:8080/hello   
http://localhost:8090/hello   


Es werden zwei Datenbanken benötigt! Es müssen aber keine Tabellen etc. angelegt werden   
Name der User Datenbank: userservice   
Name der Product Datenbank: productservice   


Aktuell sind die default-Zugangsdaten für die XAMPP MySQL Anwendung gepflegt. Solltet ihr das anpassen, denkt dran, es nicht zu commiten.   

spring.datasource.url = jdbc:mysql://localhost:3306/{{DatenbankName}}   
spring.datasource.username = root   
spring.datasource.password =   

Der Eshop lässt sich, wie bisher auch, auf dem Tomcat Server ausführen. 
Bei Änderungen 
- auf Codeebene: Muss der Tomcat neu gestartet werden. 
- der Konfiguration: Muss das Project auf dem Tomcat neu deployed werden.
